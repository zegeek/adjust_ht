# "Network Monitoring Engineer” Take Home Task
 - Author: Belhassen Dahmen
 - Last modification: 22/09/2020
### Introduction:
This repository is intended to be seen by the Adjust recruiting for the Home Tasks. Each task is related to a 
subfolder that holds its related scripts. This readme document will contain all related explanation to my proposed 
solutions.
### Task1
The folder task1 contains the following scripts:
```
    -rwxr-xr-x 1 root root 1826 Sep 22 07:21 random
    -rwxr-xr-x 1 root root 479 Sep 22 09:29 test_random
    -rwxr-xr-x 1 root root 591 Sep 22 07:18 verify_seq **random** is the main script where I implemented the 
```

algorithm that generates a random sequence between a lower and a higher integer of the user choice. The usage is 
like follow:
```
    [root@monitoringvm task1]# ./random 1 10 A sample output should be as follow too:
    [root@monitoringvm task1]# ./random 1 10
    2 8 5 7 9 6 4 10 1 3
    [root@monitoringvm task1]# ./random 1 12
    2 8 12 4 5 7 3 6 9 11 10 1 This script is based upon the built-in random functionality of the bash 
```

interpreter, *RANDOM*, from which is calculated the random number based on the following formula
`number=((RANDOM + MIN) % MAX) + MIN` 
The main script uses a function, **verify_seq**, that allows to filter any 
already used value in the sequence and then guarantee non-redundancy. The test script, **test_random** is a 
validation script that takes the output of the main script **random** and sees if there is no repetition. the 
**verify_seq** is developed as an external function in order to use it in both the main and the test script.
### Task2
SSL offloading is a task based upon encryption and decryption. Therefore, when it is a matter of calculation, ones 
must monitor the *CPU load*. This task could be accomplished, interactively using the following tools: ***nmon*** 
: nmon can be used to monitor CPU usage, and by using the *u* option, it displays the top resource using process.
***mpstat***: with mpstat, we can have an average cpu use in the user domain or per cpu, and using the script 
**mon_per_cpu** we can obtain usage per cpu. ***netstat*** and ***ps***: allows us to know which process is 
overloading the cpu. Using the script **mon_cpu_ssl** will let us know which process is trespassing a certain 
threshold. Memory also presents a limitation, for it is needed in order to allow the server to handle concurrent 
connections. Memory should be tracked by using ***nmon*** tool with the *m* option that gives us real time 
insights about memory consumptions, swapping and other important performance keys. ***free*** with the built in 
***free*** tool we can have a general idea about the server's memory. See the script **per_mem_used**
```
    ./per_mem_used
    Used memory: 0.71% NIC monitoring is very important in this case. 2 times 10Gbit/s NICs will allow to forward
```
 
in theory 1MB per transaction. Tools like ***nload*** will allow us to track the load on the different network 
interfaces in real time. We can use ***ifstat*** to track error rates like the following
```
    ifstat -e
    Interface RX Pkts/Rate TX Pkts/Rate RX Data/Rate TX Data/Rate
                 RX Errs/Rate RX Drop/Rate RX Over/Rate RX Leng/Rate
                  RX Crc/Rate RX Frm/Rate RX Fifo/Rate RX Miss/Rate
                 TX Errs/Rate TX Drop/Rate TX Coll/Rate TX Carr/Rate
                 TX Abrt/Rate TX Fifo/Rate TX Hear/Rate TX Wind/Rat
```               
**Monitoring challenges** 
Trying to cope with a very demanding environment like this one can be tricky sometimes as it needs a real time follow up in order to 
prevent bottlenecks and can't be assured by a human. A solution to this is to create an app based on scripts that 
continuously collect data on critical KPI's (using cron for example). These data can be stored in a database, and, 
using a more advanced scripting technologies, ones can use the data in order to present them in a more intuitive 
manner (Charts, reports...) or even build a predictive model with AI algorithms that enhances the effectiveness of 
the person responsible for the monitoring.
### Conclusion
I hope you liked my various ideas about how to solve this problem and my way of understanding the matter. Please 
email me if there is any issue. I am eager to hear from you soon :D

